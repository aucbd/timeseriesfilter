from typing import Callable
from typing import Generator
from typing import Iterable
from typing import TypeVar

T = TypeVar('T')
S = TypeVar('S')


def intersperse(iterable: Iterable[T], delimiters: Iterable[T]) -> Generator[T, None, None]:
    it = iter(iterable)
    dl = iter(delimiters)
    yield next(it)
    for x in it:
        yield next(dl)
        yield x


def compose(f: Callable[..., S], *args: ...) -> Callable[[T], S]:
    return lambda x: f(*args, x)


def remove_odds(it: Iterable[T]) -> Generator[T, None, None]:
    for n, x in enumerate(it):
        if not n % 2:
            yield x


def remove_even(it: Iterable[T]) -> Generator[T, None, None]:
    for n, x in enumerate(it):
        if n % 2:
            yield x
