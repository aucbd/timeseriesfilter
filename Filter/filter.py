from typing import Dict
from typing import Optional

from .parse import parse_string
from .query import to_query
from .types import FilterList
from .language import Language


class Filter:
    def __init__(self, language: Language):
        self.language: Language = language

        self.filters_string: str = ""
        self.filters: FilterList = []

        self.query_fields: Dict[str, Dict[str, Dict[str, str]]] = {}

    def from_string(self, f_string: str) -> 'Filter':
        self.filters_string = f_string
        self.filters = []

        self.filters = parse_string(self.language, f_string)

        return self

    def to_query(self, language: str, fields: Optional[Dict[str, Dict[str, str]]] = None):
        _fields = self.query_fields.get(language.lower(), fields or {})
        self.query_fields[language.lower()] = _fields

        return to_query(self.filters, _fields, language)

    def __str__(self):
        return " ".join(map(str, self.filters))

    def __repr__(self):
        return self.__str__()
