from re import match
from re import split
from re import sub
from typing import List
from typing import Optional
from typing import Union

from .language import Language
from .types import And
from .types import Block
from .types import BlockClose
from .types import BlockOpen
from .types import Equality
from .types import FilterItem
from .types import FilterList
from .types import FilterType
from .types import Is
from .types import ListAnd
from .types import ListOr
from .types import Logic
from .types import Not
from .types import Or
from .types import RangeInc
from .types import RangeLInc
from .types import RangeNonInc
from .types import RangeRInc
from .utils import compose
from .utils import remove_odds


def parse_filter_values(language: Language, v_str: str, v_field: str) -> List[str]:
    groups = match(language.pattern_filters[v_field]["groups_wildcards"], v_str).groups()
    groups = list(map(lambda t: sub(r"\*+", "*", t or "*"), groups or []))

    return groups


def parse_filter(language: Language, filter_raw: str) -> Optional[FilterItem]:
    filter_field: str = filter_raw[0]
    filter_op: Equality = Is if match(language.op_is, filter_raw[1]) else Not
    filter_type: Optional[FilterType] = None
    filter_values: List[List[str]]
    filter_str = filter_raw[2:]

    if language.pattern_filters[filter_field]["range"].match(filter_str):
        range1, range_type, range2 = split(language.pattern_split_range, filter_str)
        filter_values = [
            parse_filter_values(language, range1 or "*", filter_field),
            parse_filter_values(language, range2 or "*", filter_field)
        ]

        if match(language.op_range_inc, range_type):
            filter_type = RangeInc
        elif match(language.op_range_linc, range_type):
            filter_type = RangeLInc
        elif match(language.op_range_rinc, range_type):
            filter_type = RangeRInc
        elif match(language.op_range_noninc, range_type):
            filter_type = RangeNonInc
    elif language.pattern_filters[filter_field]["list"].match(filter_str):
        items_raw: List[str] = list(split(language.pattern_split_list, filter_str))
        items_raw = list(remove_odds(items_raw))
        filter_values = list(map(
            lambda i: parse_filter_values(language, i, filter_field),
            items_raw
        ))
        filter_type = ListOr if filter_op is Is else ListAnd
    else:
        return None

    return filter_field, filter_op, filter_type, filter_values


def parse_item(language: Language, _filter: str) -> Optional[Union[Logic, Block, FilterItem]]:
    if match(language.op_and, _filter):
        return And
    elif match(language.op_or, _filter):
        return Or
    elif match(language.op_block_open, _filter):
        return BlockOpen
    elif match(language.op_block_close, _filter):
        return BlockClose
    elif match(language.pattern_filter, _filter):
        return parse_filter(language, _filter)
    else:
        return None


def parse_string(language: Language, filters_string: str) -> FilterList:
    filters_string = sub(language.pattern_invalid_characters, "", filters_string)

    if match(language.pattern, filters_string) is None:
        return []

    filters_str: List[str] = language.pattern_split.split(filters_string)
    filters_str = list(filter(bool, filters_str))

    parse_item_comp = compose(parse_item, language)

    filters = list(
        filter(
            lambda x: x is not None,
            map(parse_item_comp, filters_str)
        )
    )

    return filters
