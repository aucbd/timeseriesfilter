from typing import List
from typing import Tuple
from typing import TypeVar
from typing import Union

Is = TypeVar('Is')
Not = TypeVar('Not')
And = TypeVar('And')
Or = TypeVar('Or')
RangeInc = TypeVar('RangeInc')
RangeLInc = TypeVar('RangeLInc')
RangeRInc = TypeVar('RangeRInc')
RangeNonInc = TypeVar('RangeNonInc')
ListAnd = TypeVar('ListAnd')
ListOr = TypeVar('ListOr')
BlockOpen = TypeVar('BlockOpen')
BlockClose = TypeVar('BlockClose')
Greater = TypeVar("Greater")
GreaterEqual = TypeVar("GreaterEqual")
Lower = TypeVar("Lower")
LowerEqual = TypeVar("LowerEqual")
Wildcard = TypeVar('Wildcard')

Block = Union[BlockOpen, BlockClose]
Logic = Union[And, Or]
Equality = Union[Is, Not]
Inequality = Union[Greater, GreaterEqual, Lower, LowerEqual]
FilterType = Union[RangeInc, RangeLInc, RangeRInc, RangeNonInc, ListAnd, ListOr]
FilterItem = Tuple[str, Equality, FilterType, List[List[str]]]
FilterList = List[Union[Logic, Block, FilterItem]]

blocks = (BlockOpen, BlockClose,)
logic = (And, Or,)
equality = (Is, Not,)
filterTypes = (RangeInc, RangeLInc, RangeRInc, RangeNonInc, ListAnd, ListOr,)
lists = (ListAnd, ListOr,)
ranges = (RangeInc, RangeLInc, RangeRInc, RangeNonInc)


def false(_type: Union[Logic, Equality, Inequality, ListAnd, ListOr]) -> Union[Equality, Inequality]:
    if _type is And:
        return Or
    elif _type is Or:
        return And
    elif _type is Is:
        return Not
    elif _type is Not:
        return Is
    elif _type is ListAnd:
        return ListOr
    elif _type is ListOr:
        return ListAnd
    elif _type is Greater:
        return LowerEqual
    elif _type is GreaterEqual:
        return Lower
    elif _type is Lower:
        return GreaterEqual
    elif _type is LowerEqual:
        return Greater
