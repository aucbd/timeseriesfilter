from json import dumps
from re import sub
from typing import Dict
from typing import List
from typing import Union

from .types import And
from .types import BlockClose
from .types import BlockOpen
from .types import FilterItem
from .types import FilterList
from .types import Greater
from .types import GreaterEqual
from .types import Inequality
from .types import Is
from .types import ListAnd
from .types import ListOr
from .types import Logic
from .types import Lower
from .types import LowerEqual
from .types import Not
from .types import Or
from .types import RangeLInc
from .types import RangeNonInc
from .types import RangeRInc
from .types import Wildcard
from .types import blocks
from .types import false
from .types import lists
from .types import logic
from .types import ranges
from .utils import compose
from .utils import intersperse

dictionary_sql: Dict[type, str] = {
    And: "AND",
    Or: "OR",
    BlockOpen: "(",
    BlockClose: ")",
    Is: "LIKE",
    Not: "!=",
    ListAnd: "AND",
    ListOr: "OR",
    Wildcard: "%",
    Greater: ">",
    GreaterEqual: ">=",
    Lower: "<",
    LowerEqual: "<=",
}


def join_value(delimiters: List[str], _type: str, _item: List[str]) -> str:
    if _type.lower() == "str":
        return dumps("".join(intersperse(_item, delimiters)))
    elif _type.lower() == "int":
        return str(int("".join(intersperse(_item, delimiters))))
    else:
        raise TypeError("Unknown type" + repr(_type))


def join_list(field_name: str, op_str: str, type_str: str, items: List[str]) -> str:
    items = map(lambda i: f"{field_name} {op_str} " + i, items)

    return f" {type_str} ".join(items)


def join_range(operators: Dict[type, str], field_name: str, _op: type, range_type: type, values: List[str]) -> str:
    range_items: List[Union[str, Inequality, Logic]] = [
        field_name,
        Greater if range_type in (RangeNonInc, RangeRInc) else GreaterEqual,
        values[0],
        And,
        field_name,
        Lower if range_type in (RangeNonInc, RangeLInc) else LowerEqual,
        values[1]
    ]

    if _op is Not:
        range_items[1] = false(range_items[1])
        range_items[3] = false(range_items[3])
        range_items[5] = false(range_items[5])

    return " ".join(map(lambda i: i if isinstance(i, str) else operators[i], range_items))


def translate_item(operators: Dict[type, str], _fields: Dict[str, Dict[str, str]], item: FilterItem) -> str:
    field, op, _type, values = item

    delimiters = x if isinstance(x := _fields[field].get("join", ""), list) else [x]
    delimiters.extend([delimiters[-1]] * (max(map(len, values)) - 1))

    replace_wildcard = compose(map, compose(
        sub,
        r"\*",
        operators[Wildcard] if _type in lists else _fields[field]["zero"]
    ))
    join_value_comp = compose(
        join_value,
        delimiters,
        _fields[field].get("type", "str")
    )

    joined_values: List[str] = list(map(join_value_comp, map(replace_wildcard, values)))

    if _type in lists:
        return join_list(_fields[field]["name"], operators[op], operators[_type], joined_values)
    elif _type in ranges:
        return join_range(operators, _fields[field]["name"], op, _type, joined_values)
    else:
        raise TypeError("Unknown type" + repr(_type))


def to_query(filters: FilterList, fields: Dict[str, Dict[str, str]], language: str) -> str:
    query: List[str] = []
    dictionary: Dict[type, str]

    if "sql" in language.lower():
        dictionary = dictionary_sql
    else:
        raise KeyError("Unknown language " + language)

    for item in filters:
        if item in logic + blocks:
            query.append(dictionary[item])
        else:
            query.append(dictionary[BlockOpen])
            query.append(translate_item(dictionary, fields, item))
            query.append(dictionary[BlockClose])

    return " ".join(query)
