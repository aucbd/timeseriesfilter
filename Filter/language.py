"""
Filter Language Specification

<field><operator><filter[list operator]>[[range operator][filter]][boolean operator]

FIELDS:
    - T     Time
    - W     Weekday
    - M     Month
    - D     Date

OPERATORS:
    - =     Equals
    - !     Not equals

FILTER:
    - Time  HH:MM(:SS(.sss)?)?
    - W.Day (Mon?|Tue?|Wed?|Thu?|Fri?|Sat?|Sun?)
    - Month (Jan?|Feb?|Mar?|Apr?|May?|Jun|Jul|Aug?|Sep?|Oct?|Nov?|Dec?)
    - Date  DD/MM/YYYY

LIST OPERATOR:
    - ,     Add item to filters as OR if = and as AND if !

RANGE OPERATORS:
    - -     Inclusive range [a;b]
    - >     Left-inclusive range [a;b)
    - <     Right-inclusive range (a;b]
    - <>    Non-inclusive range (a;b)

BOOLEAN OPERATORS:
    - &     And
    - |     Or

EXAMPLES:
    - T=10:00>12:00             From 10:00:00 to 11:59:59
    - D=01/**/****&M=Jan<>Apr   All first days of the months from January to April
    - T=10:*&W!Sat,Sun          From 10:00:00 to 10:59:59 of every day excluding Saturdays and Sundays
"""
from re import IGNORECASE
from re import Pattern
from re import compile as re_compile
from typing import Dict
from typing import List

type_filter = Dict[str, str]
type_filter_re = Dict[str, Pattern]


class Language:
    def __init__(self, fields: List[type_filter], operators: Dict[str, str]):
        self.fields: Dict[str, type_filter] = {
            f["key"]: f
            for f in [{k.lower(): v for k, v in f_.items()} for f_ in fields]
        }
        self.operators: Dict[str, str] = {k.lower(): v for k, v in operators.items()}

        self.operators_types: Dict[str, List[str]] = {}
        self.pattern_filters: Dict[str, type_filter_re] = {}
        self.pattern_split_range: Pattern = re_compile("")
        self.pattern_split_list: Pattern = re_compile("")
        self.pattern_split: Pattern = re_compile("")
        self.pattern_valid_characters: Pattern = re_compile("")
        self.pattern_invalid_characters: Pattern = re_compile("")
        self.pattern_filter: Pattern = re_compile("")
        self.pattern: Pattern = re_compile("")

        self.build()

    def __getattr__(self, item):
        if item == "op_is":
            return self.operators["is"]
        elif item == "op_not":
            return self.operators["not"]
        elif item == "op_and":
            return self.operators["and"]
        elif item == "op_or":
            return self.operators["or"]
        elif item == "op_list":
            return self.operators["list"]
        elif item == "op_range_inc":
            return self.operators["range_inc"]
        elif item == "op_range_linc":
            return self.operators["range_linc"]
        elif item == "op_range_rinc":
            return self.operators["range_rinc"]
        elif item == "op_range_noninc":
            return self.operators["range_noninc"]
        elif item == "op_block_open":
            return self.operators["block_open"]
        elif item == "op_block_close":
            return self.operators["block_close"]
        elif item == "equality":
            return self.operators_types["equality"]
        elif item == "logic":
            return self.operators_types["logic"]
        elif item == "range":
            return self.operators_types["range"]
        elif item == "list":
            return self.operators_types["list"]
        elif item == "block":
            return self.operators_types["block"]
        elif item == "split":
            return self.operators_types["split"]
        else:
            raise AttributeError(f"'{type(self).__name__}' object has no attribute '{item}'")

    def build(self):
        self.operators_types = {
            "equality": [
                self.operators["is"],
                self.operators["not"],
            ],
            "logic": [
                self.operators["and"],
                self.operators["or"],
            ],
            "range": [
                self.operators["range_inc"],
                self.operators["range_linc"],
                self.operators["range_rinc"],
                self.operators["range_noninc"],
            ],
            "list": [
                self.operators["list"]
            ],
            "block": [
                self.operators["block_open"],
                self.operators["block_close"],
            ],
            "split": [
                self.operators["and"],
                self.operators["or"],
                self.operators["block_open"],
                self.operators["block_close"],
            ]
        }

        build_valid_characters: List[str] = [
            r"A-Za-z0-9",
            r"\/:.",
            r"\*",
            *self.operators_types["equality"],
            *self.operators_types["list"],
            *self.operators_types["range"],
            *self.operators_types["logic"],
            *self.operators_types["block"],
        ]

        build_fields: Dict[str, type_filter] = {
            k: {
                k_: f_
                for k_, f_ in f.items()
                if k_ in ("base", "wildcards", "groups", "groups_wildcards")
            }
            for k, f in self.fields.items()
        }

        for field in build_fields.values():
            field["range"] = "(" + \
                             f"({field['base']})({'|'.join(self.operators_types['range'])})({field['base']})?|" + \
                             f"({field['base']})?({'|'.join(self.operators_types['range'])})({field['base']})" + \
                             ")"
            field["list"] = f"({field['wildcards']})({self.operators_types['list']}({field['wildcards']}))*"

        build_fields_patterns: List[str] = []
        for k, field in build_fields.items():
            build_fields_patterns.append(
                f"{k}" +
                f"({'|'.join(self.operators_types['equality'])})" +
                f"({field['range']}|{field['list']})"
            )

        build_language_filter: str = f"({'|'.join(build_fields_patterns)})"
        build_language: str = "^(" + \
                              f"{self.operators['block_open']}*" + \
                              f"{build_language_filter}" + \
                              f"{self.operators['block_close']}*" + \
                              f"({'|'.join(self.operators_types['logic'])}|$)" + \
                              ")+$"

        self.pattern_filters = {
            k: {
                k_: re_compile(f_str, flags=IGNORECASE)
                for k_, f_str in f.items()
            }
            for k, f in build_fields.items()
        }
        self.pattern_split_range: Pattern = re_compile(
            f"({'|'.join(self.operators_types['range'])})",
            flags=IGNORECASE
        )
        self.pattern_split_list: Pattern = re_compile(
            f"({'|'.join(self.operators_types['list'])})",
            flags=IGNORECASE
        )
        self.pattern_split: Pattern = re_compile(
            f"({'|'.join(self.operators_types['split'])})",
            flags=IGNORECASE
        )
        self.pattern_valid_characters: Pattern = re_compile(
            f"[{''.join(set(build_valid_characters))}]",
            flags=IGNORECASE
        )
        self.pattern_invalid_characters: Pattern = re_compile(
            f"[^{''.join(set(build_valid_characters))}]",
            flags=IGNORECASE
        )
        self.pattern_filter: Pattern = re_compile(
            build_language_filter,
            flags=IGNORECASE
        )
        self.pattern: Pattern = re_compile(
            build_language,
            flags=IGNORECASE
        )
